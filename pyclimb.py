#!/usr/bin/env python
import sqlite3
import argparse
import re
from datetime import datetime
from fuzzywuzzy import fuzz
from enum import Enum
from pprint import pprint

class Tables(Enum):
    ROUTES = "routes"
    ASCENTS = "ascents"
    SECTORS = "sectors"
    CRAGS = "crags"

    @classmethod
    def has_value(cls, value):
        return any(value == item.value for item in cls)

class Route(object):

    @staticmethod
    def get_add_query():
        return """INSERT INTO routes (name, sector_id, comment, grade, kind, link) VALUES(?,?,?,?,?,?)"""

    @staticmethod
    def higher():
        return "sectors"

    def __init__(self, name=None, grade=None, kind=None, comment=None, sector=None, link=None):
        self.name = name
        self.grade = grade
        self.kind = kind
        self.comment = comment
        self.sector = sector
        self.link = link

class Ascent(object):

    @staticmethod
    def get_add_query():
        return "INSERT INTO ascents (date, kind, comment, route_id) VALUES(?,?,?,?)"

    @staticmethod
    def higher():
        return "routes"

    def __init__(self, date=None, kind=None, comment=None, route_id=None):
        self.date = date
        self.kind = kind
        self.comment = comment
        self.route_id = route_id

class Sector(object):

    @staticmethod
    def get_add_query():
        return "INSERT INTO sectors (name, crag_id) VALUES(?,?)"

    @staticmethod
    def higher():
        return 'crags'

    def __init__(self, name=None, crag_id=None):
        self.name = name
        self.crag_id = crag_id

class Crag(object):
    @staticmethod
    def get_add_query():
        return "INSERT INTO crags (name, country) VALUES(?,?)"

    def __init__(self, name=None, country=None):
        self.name = name
        self.country = country

class Config(object):

    def __init__(self, cursor, connection, arguments, higher=None):
        self.cursor = cursor
        self.connection = connection
        self.arguments = arguments
        self.higher = higher

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    parser.add_argument('-q', '--query')
    parser.add_argument('-a', '--add')

    args = parser.parse_args()
    print(args)

    if(args.query and args.add):
        print("Don't do too many actions at once")

    conn, c = connect(args.file)

    config = Config(c, conn, args)

    if(args.add):
        add(config)
    else:
        pprint(execute_query(open(args.query, 'r').read(), config.cursor))

    close(config.connection)

def get_fields(obj):
    return [a for a in dir(obj) if not a.startswith('__') and not callable(getattr(obj,a))]

def add(config):
    if not Tables.has_value(config.arguments.add.lower()):
        print("woops")
        return
    else:
        kind = config.arguments.add.lower()
        if kind == Tables.ROUTES.value:
            config.higher = Route.higher()
            insert_route_interactive(config)
        elif kind == Tables.ASCENTS.value:
            config.higher = Ascent.higher()
            insert_ascent_interactive(config)
        elif kind == Tables.SECTORS.value:
            config.higher = Sector.higher()
            insert_sector_interactive(config)
        elif kind == Tables.CRAGS.value:
            insert_crag_interactive(config)

def open_query_file(query):
    return open(query, 'r').read()

def insert_ascent_interactive(config):
    ascent = create_ascent_from_input(config)

    execute_query(Ascent.get_add_query(), config.cursor, (ascent.date, ascent.kind, ascent.comment, ascent.route_id))
    pass

def insert_route_interactive(config):
    route = create_route_from_input(config)

    execute_query(Route.get_add_query(), config.cursor, (route.name, route.sector,
                               route.comment, route.grade, route.kind, route.link))

def insert_crag_interactive(config):
    crag = create_crag_from_input(config)
    execute_query(Crag.get_add_query(), config.cursor, (crag.name, crag.country))

def insert_sector_interactive(config):
    sector = create_sector_from_input(config)
    execute_query(Sector.get_add_query(), config.cursor, (sector.name, sector.crag_id))
    pass


def create_crag_from_input(config):
    crag = Crag()
    crag.name = clean_input("The name of the country")
    crag.country = clean_input("The country of the crag: ")
    return crag

def create_sector_from_input(config):
    sector = Sector()
    sector.name = clean_input("Name of sector: ")
    sector.crag_id = dereference_id('crag', config)
    return sector

def create_ascent_from_input(config):
    ascent = Ascent()

    ascent.route_id = dereference_id('route', config)
    ascent.date = datetime.strptime(clean_input("Date of ascent: "), '%Y-%m-%d')
    ascent.kind = clean_input("Kind of ascent: ", regex=r"2go|redpoint|flash|onsight|repeat", extra_cleaner=remove_all_whitespace).lower()
    ascent.comment = clean_input("Comment of ascent: ")

    return ascent

def create_route_from_input(config):
    route = Route()

    route.name = clean_input("Name of the route: ")
    route.sector = dereference_id('sector', config)
    route.grade = clean_input("Grade: ", regex = r"[0-9][abcABC]?[\+-]?$", extra_cleaner=remove_all_whitespace).lower()
    route.kind = clean_input("Kind: ", regex = r"sport$|boulder$", extra_cleaner=remove_all_whitespace).lower()
    route.comment = clean_input("Comment: ")
    route.link = clean_input("Link to extra info: ")

    return route

def remove_all_whitespace(s):
    return "".join(s.split())

def clean_input(question, regex=None, extra_cleaner=None):
    inp = input(question)

    if extra_cleaner:
        inp = extra_cleaner(inp)

    while regex and not re.match(regex, inp, flags=re.IGNORECASE):
        print("try again!")
        inp = input(question)
        if extra_cleaner:
            inp = extra_cleaner(inp)


    if inp == "":
        return None
    else:
        return inp

def dereference_id(id_of, config):
    question = "Name or id of {}: ".format(id_of)
    dereference = input(question)
    generic_id = get_id(id_of, dereference, config.cursor)

    while generic_id == None:
        print("That doesn't seem right, did you mean any of these?")
        print("id) name")
        closest = find_closest_matching(dereference, config.higher, config.cursor)
        print("\n".join("{}) {}".format(t[1], t[0]) for t in closest))
        dereference = input(question)
        generic_id = get_id(id_of, dereference, config.cursor)
    return generic_id

def find_closest_matching(name, column , cur, amount=10):
    l = get_id_list(column, cur)
    l.sort(key=lambda name_tuple: -1*fuzz.ratio(name_tuple[0], name))
    return l[:amount]

def get_id_list(column, cur):
    # not super safe but cant do table names with ?, won't have user input anyway
    query = """SELECT name, {}_id FROM {}""".format(column[:-1], column)
    return execute_query(query, cur)

def get_id(column, name, cur):
    try:
        # If you give a direct id instead of name
        i = int(name)
        query = """SELECT {0}_id FROM {0}s WHERE {0}_id == ?""".format(column)
        if not execute_query(query, cur, (i,)) == []:
            return i
        else:
            return None
    except ValueError:
        query = """SELECT {}_id FROM {}s WHERE name LIKE ?""".format(column, column)
        result = execute_query(query, cur, (name,))
        if result == []:
            return None
        else:
            return result[0][0]


def execute_query(query, cursor, arguments=None):
    if arguments:
        return cursor.execute(query, arguments).fetchall()
    else:
        print(query)
        return cursor.execute(query).fetchall()


def connect(sqlite_file):
    """ Make connection to an SQLite database file """
    conn = sqlite3.connect(sqlite_file, isolation_level=None)
    c = conn.cursor()
    return conn, c


def close(connection):
    connection.commit()
    connection.close()


if __name__ == "__main__":
    main()
